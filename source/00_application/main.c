#include <stdio.h>
#include <sys/time.h>
#include "init.h"
#include "kern.h"
#include "cPatten.h"

static unsigned int get_msec(void)
{
    struct timeval mstime;
    unsigned int ms = 0;
    gettimeofday(&mstime, NULL);
    ms = mstime.tv_sec * 1000 + mstime.tv_usec / 1000;
    return ms;
}

unsigned long long reckon_usec(void)
{
    struct timeval mstime;
    unsigned long long us = 0;
    gettimeofday(&mstime, NULL);
    us = mstime.tv_sec * 1000000 + mstime.tv_usec;
    return us;
}

static void show(void)
{
    printf("Hello varch!\r\n");
    cPatten_setMask('`');
    cPatten_showString("Varch");
    cPatten_setMask('*');
    cPatten_showString("Varch");
    cPatten_setMask('0');
    cPatten_showString("Varch");
    cPatten_setMask('#');
    cPatten_showString("Varch");
}

int main(int argc, char *argv[])
{
    show();

    if (kern_init(get_msec, 1) == KE_OK)
    {
        printf("################################################\r\n");
        printf("#####                  Kern                #####\r\n");
        printf("################################################\r\n");
    }
    else  
    {
        printf("kern init fail!\r\n");
        return 0;
    }

    init_execute();
    kern_schedule();

    return 0;
}

#ifdef _WIN32
void task_fflush(void)
{
    fflush(stdout);
}
static void fflush_init(void)
{
    task_create(1, task_fflush);
}
init_export_app(fflush_init);
#endif