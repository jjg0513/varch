/*********************************************************************************************************
 *  ------------------------------------------------------------------------------------------------------
 *  file description
 *  ------------------------------------------------------------------------------------------------------
 *         \file  console.h
 *         \unit  console
 *        \brief  This is a C language console input receiving module.
 *       \author  Lamdonn
 *      \version  1.0.0
 *      \license  GPL-2.0
 *    \copyright  Copyright (C) 2023 Lamdonn.
 ********************************************************************************************************/
#ifndef __console_H
#define __console_H

/* version infomation */

#define CONSOLE_V_MAJOR                     1
#define CONSOLE_V_MINOR                     0
#define CONSOLE_V_PATCH                     0

/* Maximum console command line length */
#define CONSOLE_LINE_MAX                    (256)

/* Debugging mode */
#define CONSOLE_DEBUG                       (0)

/**
 *  \brief The initialization function of the console module.
 *         If the 'init' module is used, export the initialization function through the 'init' module 
 *         or call it for initialization on your own
 *  \return none.
 */
void console_init(void);

#endif 
