/*********************************************************************************************************
 *  ------------------------------------------------------------------------------------------------------
 *  file description
 *  ------------------------------------------------------------------------------------------------------
 *         \file  init.h
 *        \brief  This is a C language initialize export module.
 *       \author  Lamdonn
 *      \version  1.0.0
 *      \license  GPL-2.0
 *    \copyright  Copyright (C) 2023 Lamdonn.
 ********************************************************************************************************/
#include "init.h"

#define NULL ((void*)0)

/* The following defines the first address of each level of section. The order cannot be modified. */
static void init_nop_process(void) {}
INIT_SECTION("init.0") static const init_item init_item_start  = {init_nop_process};
INIT_SECTION("init.1") static const init_item init_item1  = {init_nop_process};
INIT_SECTION("init.2") static const init_item init_item2  = {init_nop_process};
INIT_SECTION("init.3") static const init_item init_item3  = {init_nop_process};
INIT_SECTION("init.4") static const init_item init_item4  = {init_nop_process};
INIT_SECTION("init.5") static const init_item init_item5  = {init_nop_process};
INIT_SECTION("init.6") static const init_item init_item_end  = {init_nop_process};

void init_execute(void)
{
    const init_item *it = &init_item_start + 1; /* Get address from starting range */
    init_handler_t handler = NULL;
    
    /* Traverse the initialization function of the entire range and execute */
    while (it < &init_item_end) 
    {
        handler = it++->handler;
        if (handler != NULL) handler();
    }
}
