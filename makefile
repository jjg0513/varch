##################################################################################
### config
##################################################################################
CC                  = gcc
BUILT_DIR           = built
TARGET              = app

##################################################################################
### source locations
##################################################################################
WORKSPACE           = source
TESTSPACE           = test
APPLICATION_PATH    = $(WORKSPACE)/00_application
GENDATA_PATH        = $(WORKSPACE)/01_general
VSTD_PATH           = $(WORKSPACE)/02_vstd
CONTAINER_PATH      = $(WORKSPACE)/03_container
ALGORITHM_PATH      = $(WORKSPACE)/04_algorithm
PARSER_PATH         = $(WORKSPACE)/05_parser

##################################################################################
### sources and head path
##################################################################################
include $(TESTSPACE)/test.mk

INCLUDE             += -I $(APPLICATION_PATH)
INCLUDE             += -I $(GENDATA_PATH)
INCLUDE             += -I $(VSTD_PATH)
INCLUDE             += -I $(CONTAINER_PATH)
INCLUDE             += -I $(ALGORITHM_PATH)
INCLUDE             += -I $(PARSER_PATH)

SOURCES             += $(APPLICATION_PATH)/init.c
SOURCES             += $(APPLICATION_PATH)/main.c
SOURCES             += $(wildcard $(APPLICATION_PATH)/console/*.c)
SOURCES             += $(wildcard $(GENDATA_PATH)/*.c)
SOURCES             += $(wildcard $(VSTD_PATH)/*.c)
SOURCES             += $(wildcard $(CONTAINER_PATH)/*.c)
SOURCES             += $(wildcard $(ALGORITHM_PATH)/*.c)
SOURCES             += $(wildcard $(PARSER_PATH)/*.c)
SOURCES             += $(TEST_SRC)

##################################################################################
### targets and recipes
##################################################################################
OBJ_PATH      = $(BUILT_DIR)/obj
BIN_PATH      = $(BUILT_DIR)/bin
OBJS          = $(patsubst %.c, $(OBJ_PATH)/%.o, $(SOURCES))
TAR_PATH      = $(BIN_PATH)/$(TARGET)

# link
${TAR_PATH}:$(OBJS)
	$(shell mkdir -p $(dir $@))
#	@ $(CC) $(OBJS) -o $(TAR_PATH) -lm -lX11 -lpthread
	@ $(CC) $(CFLAG) $(OBJS) -o $(TAR_PATH) -lm -lpthread

# compile
$(OBJ_PATH)/%.o:%.c
	$(shell mkdir -p $(dir $@))
	@ echo "compiling $(notdir $<)"
	@ $(CC) $(CFLAG) $(INCLUDE) -c $< -o $@
	
.PHONY:clean
clean:
	@echo "remove app and objs files ..."
	$(shell rm $(BUILT_DIR)/$(WORKSPACE) -rf)
	$(shell rm $(BUILT_DIR)/* -rf)


