#!/bin/bash

exe_file="built/bin/app"

echo "cleaning..."
if [ -f $exe_file ]; then 
    rm $exe_file
fi

echo "compilling..."
make

if [ -f $exe_file ]; then 
    echo "executing..."
    ./$exe_file
else 
    echo "compile fail!!!"
fi

# find . -name *.h -o -name  *.c | xargs wc -l

# enter key to continue
read -p "Press enter to continue"
