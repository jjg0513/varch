## 介绍  

arg模块主要针对应用布丁参数的场合，可以获取不定参数的个数及指定位置的参数。  

## 接口  

### 函数

```c
#define ARG_MAX             // 支持最大的不定参数个数
#define ARGC(...)           // 当前不定参数的个数，不支持区分空参数
#define ARGC0(...)          // 当前不定参数的个数，支持区分空参数，但编译需要更多的空间，视情况选择使用
#define ARGS(x, ...)        // 获取指定位置参数
```
在使用上只需关注以上的宏定义即可，其他的均可以无需关注。

使用例子：  
```c
static void test(void)
{
    printf("ARG_MAX %d\r\n", ARG_MAX);

    printf("ARGC %d\r\n", ARGC());
    printf("ARGC %d\r\n", ARGC(A));
    printf("ARGC %d\r\n", ARGC(A, B));
    printf("ARGC %d\r\n", ARGC(A, B, C));

    printf("ARGC %d\r\n", ARGC0());
    printf("ARGC %d\r\n", ARGC0(A));
    printf("ARGC %d\r\n", ARGC0(A, B));
    printf("ARGC %d\r\n", ARGC0(A, B, C));

    printf("ARGS %s\r\n", ARGS(0, "arg0", "arg1", "arg2"));
    printf("ARGS %s\r\n", ARGS(1, "arg0", "arg1", "arg2"));
    printf("ARGS %s\r\n", ARGS(2, "arg0", "arg1", "arg2"));
}
```
结果： 
```
ARG_MAX 124
ARGC 1
ARGC 1
ARGC 2
ARGC 3
ARGC 0
ARGC 1
ARGC 2
ARGC 3
ARGS arg0
ARGS arg1
ARGS arg2
```

