## 介绍  

查找算法是用于在数据结构中查找特定元素的算法。常见的查找算法包括线性查找、二分查找、哈希查找等。下面简要介绍这些查找算法的原理和实现：

线性查找：线性查找是一种最简单的查找算法，它从数据结构的起始位置开始，逐个比较每个元素，直到找到目标元素或遍历完所有元素。线性查找的时间复杂度为O(n)，其中n为数据结构的大小。

二分查找：二分查找适用于有序的数据结构，它每次将数据结构分为两半，然后根据目标元素与中间元素的比较结果来确定继续查找的子区间。二分查找的时间复杂度为O(log n)。

哈希查找：哈希查找通过构造哈希函数将数据映射到哈希表中，然后根据哈希值直接访问目标元素。哈希查找的平均时间复杂度为O(1)。*** 在dict模块中就应用hash查找 ***

## 接口  

```c
int search_linear(void *array, int left, int right, void *target, SearchOPS *ops);
int search_binary(void *array, int left, int right, void *target, SearchOPS *ops);
```
这两种查找算法使用方法一致，都是传入数据地址、数组查找区间、查找目标和依赖于数据容器的操作集，返回查找出来的数组索引（失败将会返回负数）。 

## 测试  

```c
static void test_linear(void)
{
    int array[] = {6, 3, 1, 0, -2, 9};
    SearchOPS ops;
    int target = 1;

    ops.addr = int_array_addr;
    ops.cmp = int_cmp;

    target = 9, printf("search %d, result %d\r\n", target, search_linear(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = -2, printf("search %d, result %d\r\n", target, search_linear(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = 0, printf("search %d, result %d\r\n", target, search_linear(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = 1, printf("search %d, result %d\r\n", target, search_linear(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = 3, printf("search %d, result %d\r\n", target, search_linear(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = 6, printf("search %d, result %d\r\n", target, search_linear(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));

    target = 5, printf("search %d, result %d\r\n", target, search_linear(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = -9, printf("search %d, result %d\r\n", target, search_linear(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
}
```

结果  

```
search 9, result 5
search -2, result 4
search 0, result 3
search 1, result 2
search 3, result 1
search 6, result 0
search 5, result -1
search -9, result -1
```

```c
static void test_binary(void)
{
    int array[] = {0, 3, 6, 10, 62, 99};
    SearchOPS ops;
    int target = 10;

    ops.addr = int_array_addr;
    ops.cmp = int_cmp;

    target = 0, printf("search %d, result %d\r\n", target, search_binary(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = 3, printf("search %d, result %d\r\n", target, search_binary(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = 6, printf("search %d, result %d\r\n", target, search_binary(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = 10, printf("search %d, result %d\r\n", target, search_binary(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = 62, printf("search %d, result %d\r\n", target, search_binary(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = 99, printf("search %d, result %d\r\n", target, search_binary(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));

    target = 5, printf("search %d, result %d\r\n", target, search_binary(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = -9, printf("search %d, result %d\r\n", target, search_binary(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
}
```

结果  

```
search 0, result 0
search 3, result 1
search 6, result 2
search 10, result 3
search 62, result 4
search 99, result 5
search 5, result -1
search -9, result -1
```