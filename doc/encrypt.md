## 介绍  

在数据传输当中有些敏感的数据需要进行加密来保证安全，比如：用户名密码。    
加解密算法一般分为：**对称加密算法** 和 **非对称加密算法**。  
这个模块提供了一些基本的加解密算法：    
- [x] DES 加解密  
- [x] DES3 加解密
- [ ] AES 加解密
- [ ] SHA1 加密
- [ ] MD5 加密
- [ ] HMAC 计算
- [ ] blowfish 加解密
- [ ] RSA 加解密、公钥、签名、转换

## 接口  

### DES  
```c
int des_set_key(const uint8_t key[8]);
int des_crypt_ecb(const uint8_t input[8], uint8_t output[8], uint8_t mode);
int des_crypt_cbc(const uint8_t *input, uint8_t *output, uint32_t length, uint8_t mode);
```
DES算法常见ECB和CBC模式，ECB模式下每一个加密块独立进行运算，而CBC模式是会依赖前一个加密块进行运算。  
在进行加解密之前，需要先设置密钥`des_set_key`，密钥是默认8个字节。  
`des_crypt_ecb`加解密算法都是用这个函数，通过`mode`参数设置为加密还是解密。  
`des_crypt_cbc`与`des_crypt_ecb`不同，`input`和`output`长度需一样而是8的倍数，通过`length`传入。  

例子：  
```c
void test_des(void)
{
	uint8_t key[8] = "hello";
	uint8_t data_block[8] = {1, 2, 3, 4, 5, 6, 7, 9};
	uint8_t processed_block[8];
	int i;

	des_set_key(key);

	printf("des ecb encrypt: ");
	des_crypt_ecb(data_block, processed_block, DES_ENCRYPT);
	for (i = 0; i < 8; i++)
	{
		printf("%d ", processed_block[i]);
	}
	printf("\r\n");

	printf("des ecb decrypt: ");
	des_crypt_ecb(processed_block, data_block, DES_DECRYPT);
	for (i = 0; i < 8; i++)
	{
		printf("%d ", data_block[i]);
	}
	printf("\r\n");

	///////////////////////////////////////
	printf("des cbc encrypt: ");
	des_crypt_cbc(data_block, processed_block, 8, DES_ENCRYPT);
	for (i = 0; i < 8; i++)
	{
		printf("%d ", processed_block[i]);
	}
	printf("\r\n");

	printf("des cbc decrypt: ");
	des_crypt_cbc(processed_block, data_block, 8, DES_DECRYPT);
	for (i = 0; i < 8; i++)
	{
		printf("%d ", data_block[i]);
	}
	printf("\r\n");
}
```
结果：  
```
des ecb encrypt: 156 151 171 0 235 148 83 44 
des ecb decrypt: 1 2 3 4 5 6 7 9 
des cbc encrypt: 156 151 171 0 235 148 83 44 
des cbc decrypt: 1 2 3 4 5 6 7 9
```

### DES3  
```c
int des3_set_key2(const uint8_t key[16]);
int des3_set_key3(const uint8_t key[24]);
int des3_crypt_ecb(const uint8_t input[8], uint8_t output[8], uint8_t mode);
int des3_crypt_cbc(const uint8_t *input, uint8_t *output, uint32_t length, uint8_t mode);
```
在用法上`DES3`和`DES`是一致的，是对des三次加密，密钥的长度也相应增长，可以设置为16字节和24字节的密钥。其他`ECB`和`CBC`的加解密模式用法和`DES`是一样的。  
例子：
```c
void test_des3(void)
{
	uint8_t key[24] = "hello world";
	uint8_t data_block[8] = {1, 2, 3, 4, 5, 6, 7, 9};
	uint8_t processed_block[8];
	int i;

	des3_set_key2(key);

	printf("des3 ecb encrypt: ");
	des3_crypt_ecb(data_block, processed_block, DES_ENCRYPT);
	for (i = 0; i < 8; i++)
	{
		printf("%d ", processed_block[i]);
	}
	printf("\r\n");

	printf("des3 ecb decrypt: ");
	des3_crypt_ecb(processed_block, data_block, DES_DECRYPT);
	for (i = 0; i < 8; i++)
	{
		printf("%d ", data_block[i]);
	}
	printf("\r\n");

	///////////////////////////////////////
	printf("des3 cbc encrypt: ");
	des3_crypt_cbc(data_block, processed_block, 8, DES_ENCRYPT);
	for (i = 0; i < 8; i++)
	{
		printf("%d ", processed_block[i]);
	}
	printf("\r\n");

	printf("des3 cbc decrypt: ");
	des3_crypt_cbc(processed_block, data_block, 8, DES_DECRYPT);
	for (i = 0; i < 8; i++)
	{
		printf("%d ", data_block[i]);
	}
	printf("\r\n");
}
```
结果：  
```
des3 ecb encrypt: 48 251 201 178 251 3 6 54 
des3 ecb decrypt: 1 2 3 4 5 6 7 9 
des3 cbc encrypt: 48 251 201 178 251 3 6 54 
des3 cbc decrypt: 1 2 3 4 5 6 7 9 
```