## 介绍  

cPatten为一个简单的字符艺术字模块，通过字符组成艺术字符。  

## 接口  

### 函数

```c
int cPatten_setMask(char c);            // 设置艺术字组成字符
void cPatten_showChar(char c);          // 显示艺术字符
void cPatten_showString(char *s);       // 显示艺术字符串
```

使用例子：  
```c
static void show(void)
{
    cPatten_setMask('`');
    cPatten_showString("Varch");
    cPatten_setMask('*');
    cPatten_showString("Varch");
    cPatten_setMask('0');
    cPatten_showString("Varch");
    cPatten_setMask('#');
    cPatten_showString("Varch");
}
```
结果： 
```
 `     `   ```   ```     ```   `   `
 `     `  `   `  `   `  `   `  `   `
  `   `   `````  ```    `      `````
   ` `    `   `  `  `   `   `  `   `
    `     `   `  `   `   ```   `   `
 *     *   ***   ***     ***   *   *
 *     *  *   *  *   *  *   *  *   *
  *   *   *****  ***    *      *****
   * *    *   *  *  *   *   *  *   *
    *     *   *  *   *   ***   *   *
 0     0   000   000     000   0   0
 0     0  0   0  0   0  0   0  0   0
  0   0   00000  000    0      00000
   0 0    0   0  0  0   0   0  0   0
    0     0   0  0   0   000   0   0
 #     #   ###   ###     ###   #   #
 #     #  #   #  #   #  #   #  #   #
  #   #   #####  ###    #      #####
   # #    #   #  #  #   #   #  #   #
    #     #   #  #   #   ###   #   #

```

