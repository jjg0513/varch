## 介绍  

控制台模块在程序开发和运行中扮演着关键角色，主要用于管理基于字符的应用程序的输入输出。通过控制台模块，开发者能够实现与用户的有效交互，同时对命令行界面进行定制和管理。  

## 接口  

### 函数

```c
void console_init(void);
```

控制台模块初始化模块函数，可以手动调用，也可以通过init模块导出调用（默认通过导出调用），程序运行后直接使用即可。

使用例子：  
```
>> cmd -h

Usage:
Enter the command line to execute the corresponding command

OPTIONS
[-l] : Print currently supported commands
[-n] : Print the number of currently supported commands
[-h] : Print help
[-v] : Print version
[-c] : Print the configuration information of the current command module
       `argc`  : The maximum number of parameters supported for parsing in the input command
       `line`  : The maximum length supported for parsing in the input command
       `count` : The maximum command count supported
>>
>>
>> cmd -l

command list:
@ cmd
@ into
>>
>>

```


