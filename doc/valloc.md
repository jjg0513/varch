## 介绍  

内存动态分配是指在程序运行过程中，根据程序的需要动态地分配内存空间，以便存储数据或创建对象。内存动态分配通常使用指针来实现，通过调用系统提供的内存分配函数（如malloc、calloc等）来申请内存空间，申请成功后，返回一个指向该内存空间的指针，进而在程序中使用该指针来访问分配的内存空间。

内存动态分配的优点是可以动态地分配内存空间，避免浪费，提高内存使用效率。同时，动态分配的内存空间也可以随着程序的需求进行动态的释放，避免内存泄漏和出错。但是，使用内存动态分配也存在一些缺点，如容易出现内存泄漏、空间碎片等问题，需要程序员具有一定的技巧和注意事项才能正确、高效地使用内存动态分配。

这里，介绍一种可以检查动态内存使用情况的小工具`valloc`，以便统计动态内存的使用情况，以及检查是否有空间碎片忘记释放。用其方法代替的内存分配方法，可以计算动态内存使用的情况。    

## 接口  

### 检查未释放内存

```c
void v_check_unfree(void);
```

### 获取已分配内存块计数

```c
int v_check_count(void);
```

### 获取已使用内存总大小

```c
int v_check_used(void);
```

## 使用  

在使用上非常简单，如下几步。

**1** 在需要统计的源文件中添加 `valloc.h` 头文件，添加到 `stdlib.h` 后面，不管哪一行只要在其后面，`valloc.h`中的内存函数（`malloc()等`）会将`stdlib.h`中的覆盖掉，而使用`valloc.h`的函数。  

**2** 在需要输出的地方，调用 valloc 的API即可。  

### 例子 

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "valloc.h"

static void test(void)
{
    void* p = NULL;
    printf("malloc %p\r\n", malloc(0));
    p = realloc(NULL, 100);
    p = malloc(64);
    p = malloc(50);
    printf("realloc %p\r\n", realloc(NULL, 0));
    printf("%d\r\n", *(int *)p);
    free(p);

    v_check_unfree();
    printf("count = %d\r\n", v_check_count());
    printf("used = %d\r\n", v_check_used());
}
init_export_app(test);
```
结果： 
```
malloc 007B5B50
realloc 007B5C20
8086040
address: 007B5B50, size: 0, file: test/test_valloc.c, line: 10
address: 007B5B88, size: 100, file: test/test_valloc.c, line: 11
address: 007B5C20, size: 0, file: test/test_valloc.c, line: 14
address: 007B5C68, size: 64, file: test/test_valloc.c, line: 12
count = 4
used = 164
```
