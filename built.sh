#!/bin/bash

exe_file="built/bin/app"

echo "cleaning..."
if [ -f $exe_file ]; then 
    rm $exe_file
fi

echo "compilling..."

if [ "$1" == "debug" ]; then
    make CFLAG=-g
else 
    make
fi

if [ -f $exe_file ]; then 
    echo "#################################"
    echo "# Compile success !!!..."
    echo "#################################"
else 
    echo "#################################"
    echo "*** Compile fail ***"
    echo "#################################"
fi

# find . -name *.h -o -name  *.c | xargs wc -l

# enter key to continue
read -p "Press enter to continue"
