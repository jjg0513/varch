#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "tool.h"
#include "valloc.h"
#include "list.h"
#include "vector.h"

static void test(void)
{
    list_t list = list(int); // 定义并创建int型list
    int i = 0;
    unsigned long long start = 0;
    unsigned long long stop = 0;

    for (i = 0; i < 10000; i++) list_push_back(list, NULL); // 依次插入 0 1 2

    // 正向遍历用时
    start = reckon_usec();
    for (i = 0; i < list_size(list); i++)
    {
        list_at(list, int, i);
    }
    stop = reckon_usec();
    printf("use %llu us!\r\n", (stop - start));

    // 反向遍历用时
    start = reckon_usec();
    for (i = list_size(list) - 1; i >= 0; i--)
    {
        list_at(list, int, i);
    }
    stop = reckon_usec();
    printf("use %llu us!\r\n", (stop - start));

    // 随机访问用时
    start = reckon_usec();
    for (i = 0; i < list_size(list); i++)
    {
        list_at(list, int, rand()%list_size(list));
    }
    stop = reckon_usec();
    printf("use %llu us!\r\n", (stop - start));


    printf("size %d\r\n", list_size(list));

    // 结束使用该list后就删除
    _list(list);

    vector_t vt = vector(int, 10000);

    // 正向遍历用时
    start = reckon_usec();
    for (i = 0; i < vector_size(vt); i++)
    {
        vector_at(vt, int, i);
    }
    stop = reckon_usec();
    printf("use %llu us!\r\n", (stop - start));

    _vector(vt);
    
    v_check_unfree();
}
init_export_app(test);
