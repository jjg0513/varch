#include "vlog.h"
#include "init.h"
#include <stdio.h>

static void vlog_callback(char *buf, int len)
{
    printf("vlog_callback[%d]: %s", len, buf);
}

static void test_vlog(void)
{
    vlog(VLOG_CHANNEL_0,   "[VLOG_CHANNEL_0] vlog!\r\n");
    vlog(VLOG_CHANNEL_1,   "[VLOG_CHANNEL_1] vlog!\r\n");
    vlog(VLOG_CHANNEL_2,   "[VLOG_CHANNEL_2] vlog!\r\n");
}

static void test_channel(void)
{
    VLOG_DISABALE(VLOG_CHANNEL_0);
    VLOG_ENABALE(VLOG_CHANNEL_1);

    vlog(VLOG_CHANNEL_0,   "[VLOG_CHANNEL_0] vlog!\r\n");
    vlog(VLOG_CHANNEL_1,   "[VLOG_CHANNEL_1] vlog!\r\n");
    vlog(VLOG_CHANNEL_2,   "[VLOG_CHANNEL_2] vlog!\r\n");
}

static void test_offline(void)
{
    vlog_set_offline(VLOG_CHANNEL_0, "./test/file/log.txt");

    for (int i = 0; i < 10; i++)
    {
        vlog(VLOG_CHANNEL_0, "vlog %d!\r\n", i);
    }

    vlog_set_offline(VLOG_CHANNEL_0, NULL);
}

static void test(void)
{
    // vlog_set_func(vlog_callback);
    // test_vlog();
    // test_channel();
    test_offline();
}
init_export_app(test);