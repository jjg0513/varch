#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "tool.h"
#include "valloc.h"
#include "dList.h"

static void test_create(void)
{
    dList *list = NULL;

    list = dList_create();
    if (!list)
    {
        printf("dList_create Fail!\r\n");
        return;
    }

    printf("dList_create Success!\r\n");

    dList_delete(list);
}

static void test_set(void)
{
    dList *list = NULL;
    int dataInt = 3;
    char *dataString = "Hello dList";

    list = dList_create();
    if (!list)
    {
        printf("dList_create Fail!\r\n");
        return;
    }

    printf("dList_create Success!\r\n");

    dList_set(list, &dataInt, sizeof(dataInt));
    printf("list->data %d\r\n", dList_ref(list, int));

    dList_set(list, dataString, strlen(dataString) + 1);
    printf("list->data %s\r\n", ((char *)(list->data)));

    dList_delete(list);
}

static void test_insert(void)
{
    dList *list = NULL;

    for (int i = 0; i < 2; i++)
    {
        if (!dList_insert(&list, -1, &i, sizeof(i))) goto FAIL;
    }

    int i = 100;
    if (!dList_insert(&list, -1, &i, sizeof(i))) goto FAIL;

    dList_forEachForward(list, n)
    {
        printf("data %d\r\n", dList_ref(n, int));
    }

    printf("------------\r\n");

    dList_forEachReverse(list, n)
    {
        printf("data %d\r\n", dList_ref(n, int));
    }

FAIL:
    dList_delete(list);
}

static void test_erase(void)
{
    dList *list = NULL;

    for (int i = 0; i < 5; i++)
    {
        if (!dList_insert(&list, -1, &i, sizeof(i))) goto FAIL;
    }

    dList_erase(&list, 0, NULL);

    dList_forEachForward(list, n)
    {
        printf("data %d\r\n", dList_ref(n, int));
    }

FAIL:
    dList_delete(list);
}

static void test_attach(void)
{
    dList *list = NULL, *a = NULL;

    for (int i = 0; i < 5; i++)
    {
        if (!dList_pushBack(&list, &i, sizeof(i))) goto FAIL;
    }
    for (int i = 0; i < 3; i++)
    {
        if (!dList_pushBack(&a, &i, sizeof(i))) goto FAIL;
    }

    dList_attach(&list, -1, a);

    dList_forEachForward(list, n)
    {
        printf("data %d\r\n", dList_ref(n, int));
    }

FAIL:
    dList_delete(list);
}

static void test_detach(void)
{
    dList *list = NULL, *node = NULL;

    for (int i = 0; i < 10; i++)
    {
        if (!dList_insert(&list, -1, &i, sizeof(i))) goto FAIL;
    }

#if 1
    node = dList_detach(&list, 0, 3, NULL);
    if (!node)
    {
        printf("dList_detach fail\r\n");
    }
#endif 

    dList_forEachForward(node, n)
    {
        printf("node data %d\r\n", dList_ref(n, int));
    }

    dList_delete(node);

    dList_forEachForward(list, n)
    {
        printf("data %d\r\n", dList_ref(n, int));
    }

FAIL:
    dList_delete(list);
}

static void test_push(void)
{
    dList *list = NULL;

    for (int i = 0; i < 5; i++)
    {
        if (!dList_pushFront(&list, &i, sizeof(i))) goto FAIL;
    }
    for (int i = 0; i < 5; i++)
    {
        if (!dList_pushBack(&list, &i, sizeof(i))) goto FAIL;
    }

    dList_forEachForward(list, n)
    {
        printf("data %d\r\n", dList_ref(n, int));
    }

FAIL:
    dList_delete(list);
}

static void test_pop(void)
{
    dList *list = NULL;

    for (int i = 0; i < 5; i++)
    {
        if (!dList_pushFront(&list, &i, sizeof(i))) goto FAIL;
    }
    for (int i = 0; i < 5; i++)
    {
        if (!dList_pushBack(&list, &i, sizeof(i))) goto FAIL;
    }

    dList_popBack(&list);
    dList_popBack(&list);
    dList_popFront(&list);

    dList_forEachForward(list, n)
    {
        printf("data %d\r\n", dList_ref(n, int));
    }

FAIL:
    dList_delete(list);
}

static void test_to(void)
{
    dList *list = NULL, *node;

    for (int i = 0; i < 10; i++)
    {
        if (!dList_pushBack(&list, &i, sizeof(i))) goto FAIL;
    }

    node = dList_to(list, -6);
    if (!node)
    {
        printf("dList_to fail\r\n");
        goto FAIL;
    }

    printf("dList_to data %d\r\n", dList_ref(node, int));

    dList_forEachForward(list, n)
    {
        printf("data %d\r\n", dList_ref(n, int));
    }

FAIL:
    dList_delete(list);
}

static void test_size(void)
{
    dList *list = NULL;

    for (int i = 0; i < 10; i++)
    {
        if (!dList_pushBack(&list, &i, sizeof(i))) goto FAIL;
    }

    printf("size %d\r\n", dList_size(list));
    printf("size %d\r\n", dList_size(dList_to(list, 3)));

FAIL:
    dList_delete(list);
}

static void test_append(void)
{
    dList *list = NULL, *ap = NULL;

    for (int i = 0; i < 10; i++)
    {
        if (!dList_pushBack(&list, &i, sizeof(i))) goto FAIL;
        if (!dList_pushBack(&ap, &i, sizeof(i))) goto FAIL;
    }

    if (!dList_append(list, &ap)) goto FAIL;

    printPoint(ap);

    dList_forEachForward(list, n)
    {
        printf("data %d\r\n", dList_ref(n, int));
    }

FAIL:
    dList_delete(list);
    dList_delete(ap);
}

static void test_copy(void)
{
    dList *list = NULL, *copy = NULL;

    for (int i = 0; i < 10; i++)
    {
        if (!dList_pushBack(&list, &i, sizeof(i))) goto FAIL;
    }

    copy = dList_copy(list, -5, -1);
    if (!copy)
    {
        printf("dList_copy fail\r\n");
    }

    dList_forEachForward(copy, n)
    {
        printf("data %d\r\n", dList_ref(n, int));
    }

FAIL:
    dList_delete(list);
    dList_delete(copy);
}

static void test_reverse(void)
{
    dList *list = NULL;

    for (int i = 0; i < 10; i++)
    {
        if (!dList_pushBack(&list, &i, sizeof(i))) goto FAIL;
    }

    if (!dList_reverse(list, 1, 5))
    {
        printf("dList_reverse fail\r\n");
    }

    dList_forEachForward(list, n)
    {
        printf("data %d\r\n", dList_ref(n, int));
    }

FAIL:
    dList_delete(list);
} 

static void test(void)
{
    printf("dList test!\r\n");

    // test_create();
    // test_set();
    // test_insert();
    // test_erase();
    // test_attach();
    // test_detach();
    // test_push();
    // test_pop();
    test_to();
    // test_size();
    // test_append();
    // test_copy();
    // test_reverse();
    
    v_check_unfree();
}
init_export_app(test);
