#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "arg.h"

static void test(void)
{
    printf("ARG_MAX %d\r\n", ARG_MAX);

    printf("ARGC %d\r\n", ARGC());
    printf("ARGC %d\r\n", ARGC(A));
    printf("ARGC %d\r\n", ARGC(A, B));
    printf("ARGC %d\r\n", ARGC(A, B, C));

    printf("ARGC %d\r\n", ARGC0());
    printf("ARGC %d\r\n", ARGC0(A));
    printf("ARGC %d\r\n", ARGC0(A, B));
    printf("ARGC %d\r\n", ARGC0(A, B, C));

    printf("ARGS %s\r\n", ARGS(0, "arg0", "arg1", "arg2"));
    printf("ARGS %s\r\n", ARGS(1, "arg0", "arg1", "arg2"));
    printf("ARGS %s\r\n", ARGS(2, "arg0", "arg1", "arg2"));
}
init_export_app(test);
