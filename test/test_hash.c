#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include "init.h"
#include "tool.h"
#include "valloc.h"
#include "hash.h"

static void test(void)
{
    printf("hash_bkdr 0x%X\r\n", hash_bkdr("Hello", 5));
    printf("hash_ap 0x%X\r\n", hash_ap("Hello", 5));
    printf("hash_djb 0x%X\r\n", hash_djb("Hello", 5));
    printf("hash_js 0x%X\r\n", hash_js("Hello", 5));
    printf("hash_rs 0x%X\r\n", hash_rs("Hello", 5));
    printf("hash_sdbm 0x%X\r\n", hash_sdbm("Hello", 5));
    printf("hash_pjw 0x%X\r\n", hash_pjw("Hello", 5));
    printf("hash_elf 0x%X\r\n", hash_elf("Hello", 5));
    printf("hash_dek 0x%X\r\n", hash_dek("Hello", 5));
    printf("hash_bp 0x%X\r\n", hash_bp("Hello", 5));
    printf("hash_fnv 0x%X\r\n", hash_fnv("Hello", 5));
    printf("hash_jdk6 0x%X\r\n", hash_jdk6("Hello", 5));
}
init_export_app(test);
