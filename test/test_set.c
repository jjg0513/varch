#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "tool.h"
#include "valloc.h"
#include "set.h"

static void test_it(void)
{
    set_t set = set(int);
    int i, index;
    void *data;

    i = -100; set_insert(set, i, &i);
    i = 1024; set_insert(set, i, &i);
    i = 0; set_insert(set, i, &i);
    i = 7; set_insert(set, i, &i);
    i = -2; set_insert(set, i, &i);
    i = -2; set_insert(set, i, &i);

    set_at(set, int, 3) = 100;

    set_it_init(set, SET_HEAD);
    i = set_size(set);
    while (i--)
    {
        data = set_it_get(set, &index);
        printf("set[%d] = %d\r\n", index, *(int *)data);
    }

    _set(set);
}

static void test_set(void) 
{
    set_t set = set(int);
    int i;

    for (i = 0; i < 100; i++)
    {
        set_insert(set, i, &i);
    }
    printf("size = %d, data size = %d\r\n", set_size(set), set_dsize(set));

    i = -100; set_insert(set, i, &i);
    i = 1024; set_insert(set, i, &i);

    printf("set[6] = %d\r\n", set_at(set, int, 6));
    printf("set[-100] = %d\r\n", set_at(set, int, -100));
    printf("set[1024] = %d\r\n", set_at(set, int, 1024));

    set_at(set, int, 6) = 11111;
    printf("set[6] = %d\r\n", set_at(set, int, 6));

    _set(set);
}

static void test(void) 
{
    // test_set();
    test_it();
    v_check_unfree();
}
init_export_app(test);
