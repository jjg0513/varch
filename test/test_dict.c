#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "tool.h"
// #include "valloc.h"
#include "dict.h"

#if 0
static void dict_print(dict_t dict)
{
    char *key;
    void *value;
    void *error;
    if (!dict) return;
    dict_it_init(dict);
    error = dict_error(dict);
    while (1)
    {
        value = dict_it_get(dict, &key);
        if (value == error) break;
        printf("key: %s,\tvalue: %d\r\n", key, *(int *)value);
    }
}

#define TEST_COUNT 1000
static void test_performance(void)
{
#if 0
    char name[10];
    int len;
    int i, j;
    printf("char *name[%d] = {", TEST_COUNT);
    for (i = 0; i < TEST_COUNT; i++)
    {
        len = rand() % 9 + 1;
        for (j = 0; j < len; j++)
        {
            name[j] = rand() % ('\\' - '0') + '0';
        }
        name[j] = 0;

        printf("\"%s\", ", name);
    }
    printf("};\r\n");
#else 
    // char *name[100] = {":A", "1?FLI1N7", "37JD:H", "[8", "QJ6F[3[", "B:>MOM", "S:MEUG0E", "D76Y6S", "XA6V@MUU4", "L9:=U10", "Z", "F", "RHWZ7<4", "TDS[>7L8D", "O", "VZ", "OC>N", "KM", "BM8@<E", "NQA", "8O4=?I", "35SEC[@", "5:A:3;", "PKL", "[?Z@T7HYX", "ZUV", "M?[L88Q", "IDF<@N", "0W?S=O", "XO1T3CNY", "?QYCYJL?7", "=DX[SA>", "8LR<0E", "U", "8", "B7R7", "9H", "=?RVMN:", "Y3WH<[>0[", "R<<B", "NI1=JNUPD", "Z>=L0V24", "JT8@T7K", "D?", "71F9R4?", "US<;I1XJ", "Z640Z<@R", "C=TSR[<", "LGA0J", ";VY=", "=?OO", "T", "CH", "MXCZPN88B", "TY><H0", "VAU:78ZV5", "2H:", "M7DL?6<G1", "@ZGLF[L", "AY3HI", "VNOXN", "B@IVD@EP", "FZPDETC", "E<", "BS>0", "860IUB[", "C=5TR", "8", ";XMQP", "NMT0", "K>0XZ6AG[", "FLOTQ[23K", ">GDGO0", "ATM", "W[", "TZGMY[X", "K3PU", "R@", "16LEN@Y", "9", "1JMD", "YBZJ", "YF", "1J0", "4:N:<", ">9Z", "7>", "@SAE;TV", ";DZ8CNM", "6", "1@5", "4", "2R@8Y60Q", "UOMNI;Y29", "YW;B1N", "U", "K", "NMPF=5", "=>", "717=KBN5", };
    char *name[1000] = {":A", "1?FLI1N7", "37JD:H", "[8", "QJ6F[3[", "B:>MOM", "S:MEUG0E", "D76Y6S", "XA6V@MUU4", "L9:=U10", "Z", "F", "RHWZ7<4", "TDS[>7L8D", "O", "VZ", "OC>N", "KM", "BM8@<E", "NQA", "8O4=?I", "35SEC[@", "5:A:3;", "PKL", "[?Z@T7HYX", "ZUV", "M?[L88Q", "IDF<@N", "0W?S=O", "XO1T3CNY", "?QYCYJL?7", "=DX[SA>", "8LR<0E", "U", "8", "B7R7", "9H", "=?RVMN:", "Y3WH<[>0[", "R<<B", "NI1=JNUPD", "Z>=L0V24", "JT8@T7K", "D?", "71F9R4?", "US<;I1XJ", "Z640Z<@R", "C=TSR[<", "LGA0J", ";VY=", "=?OO", "T", "CH", "MXCZPN88B", "TY><H0", "VAU:78ZV5", "2H:", "M7DL?6<G1", "@ZGLF[L", "AY3HI", "VNOXN", "B@IVD@EP", "FZPDETC", "E<", "BS>0", "860IUB[", "C=5TR", "8", ";XMQP", "NMT0", "K>0XZ6AG[", "FLOTQ[23K", ">GDGO0", "ATM", "W[", "TZGMY[X", "K3PU", "R@", "16LEN@Y", "9", "1JMD", "YBZJ", "YF", "1J0", "4:N:<", ">9Z", "7>", "@SAE;TV", ";DZ8CNM", "6", "1@5", "4", "2R@8Y60Q", "UOMNI;Y29", "YW;B1N", "U", "K", "NMPF=5", "=>", "717=KBN5", ";2BZX", "5;QAV7G[", "1", "X62:9M<TB", ";4DR", "Z9R>4", "I=PX", ";", ";Q47WKV4P", "A", "YLOUVG;", "R18", "2Y>Z", "1>", "RV=NE7:", "9KA", "O", "CJ4", "5", "7QC9OQL5S", ":1<", "7RJ", "0", "EUAMAT", "EZTS5", ";?=D[VOAH", "5", "XWJR", "B", "L;A244", "2X;LZZ", ";BB", "6;", "?", "NOPPF3", ":PV3:Z", "MDGX", "FV", "9QAS", "4Y:E3Y9S", "PBIX<4J", "O<", ";L79[6B", "0M34JQ1M", "NEFC?", "?K8ZQ?N?", "RF;QM6YMS", "9B9;G@", "0V@?", "8", "1J7A", "GCFE7<6=M", "H6EB?8Z", "5", "EZRWJL@8", "9XSMB<<", "WIT", "IZG0QOZ", "=@B;", "QU;JEGSU", "X;28VA960", "HGS=OQN0", "I;8>HC@E", "8>PHI:QW", "Z149I", "0?R91RSJ", "CAEVQZ<", "<DZ>6", "9R5O>SPBS", "8E", "CP7X7H", "1RTQ7595", "SP4J>;@50", "@GCU4VIP:", "P49", "AZNZK;UC", "A", ">M", "W54;L[59U", "YGW", "4HLE[", "W21PY", "5V;R86?", "V0", "[M", "IJGXO", "E?H", "5C", "KKX4", "RT[ZB?N<S", "ODY", "FE9MH61I2", "D194YR", "7J<6D3T", "V:XR7MPLW", "MEW:HE", "IORZE35", "TO[WH8", ":5DA:9F", "373ZUKWGL", "R2G=", "2I", "FY?N", "I", "KZF<X3SG6", "@9M@UO6W;", "GQY>;V", "B1:M4", "KUI7L4=VE", "[=<NWL", "R77OYB", "JT2TZ", "XI", "EP42FB", "EA<", "47UY", "D4IAG6", "SM;Q<8", "P5[<7Y", "J", "HV4LEY2", "B6DS52;", "7F6C6AK<A", "W>", "F9KT=PR:R", "4X", "XF37C;5", "NPC>0T", "?JQ=STJ", "H", ";>E?O2", "95V4@D538", "UL<G[WPQ6", "?7I7B", "LQ2NE", ";", ";4;UKCUD3", "CGE7", "K7G", "PNL4>BJD", ">76I;BB", "=P", "AV9@?", "4B0KMP==<", "7W[", "9J", ":VG58T", "[", "781", "E5@E", "EMY;B0B=", "AZ2F8@1", "0=", "[C", "L", "7", "202NE7L", "IL", "VH0<2Z", "VCNVPQV7", "Z46GJ6I<3", "XTR", "W41?A3=", "YPD;X9I4", "GL>2", "D3Z", "P?SZT62", "EW", "RM=F60C?", "[G53", "LQ9779?", "0", ">KTK4O[?", "<=[=8R?", "RXN", "HW=83FG", "Z=T15C", "<CY57:HXC", ";2I894", "4AT7?@9R", "E;:XUFSRL", "18?X[KZH", "7MCPF", "W>4HH91S", "BM", "9", "1LXN", "<M3ON>>S", "2QYZ3EZ3N", "?I>D[[?0L", "N40", "7OFY2=J42", "FJ", "Y58ADQ7X", "6<9:D", ">D7Y8", ">NBYR", "EQ[ZN46", "1@N", "HTM93I1U", "8", "N>5@8", "JI[?HBE", "H06@6", "A[M", "F:3SP<2", "2LR:", "A<J8T0NA", "N[AU", ":<", "<:>T1", "H", "EJ8P<>50", "=UX[NY6", "RCMD", "=", "A30", "6HB4JJT?Y", "?=", "L:", "?K<EUO6", "VCEOFZI5Z", "91J1T[", "K=", "<[N", "JZ0C568C1", "6[3P4", "8Q3RRWRLF", "Q:FWQ4VQ[", "W7?", "<E@@MDV", ":Y4D<", "5RU>E?9N", "GDV", "SER8", ";HI", "DCNN", "RK4", "P?YF", "P7ZKOBE", "NBX", "LKV9D>5", "E", "A4", "F<=WR", "4Y[", "4B", ":H", "JBO", "<A6JZ=", "?OKUMWJ0", "K4GK<", "EY>1D@", "827199[", "=>5@LR<", ";", ">?XY3", "B0RCE6>59", "JVNITC?Z", "[8HNCYD", ">A>R;S", "NP6D=?YW", "3DF[;X", "O4?U0P=;F", "1PY<", "30CG=", "XLXCHV", "TRMGVD@V", "MI7S3X8", "L;TG:IR", ":6I:0D2", "514", "U", ";Z", "VQGRY?", "LKB", "8LDVR25:Z", "03MI5Z0T", "DO", "0YH?T8Z", "GJ", "[E", "E2", "C", "Q[R>49>", "6R4PR1<", ">Y[1DJ", "WGV", "17<6X;A:", "JHQ8RUXI>", "JLJJ5FL", "A7<:LCG", "SR7", "J", "1SAT4", "UC[W31E", "GIK@8W", "T?", "3", "T:L", "X5N=", "S3", ":FJQ", "GBF51", "QAZJ8", "MRT?", "7;0V", "U", "HHS", "JHF5C1", "DTDUR", "1:7TFZNNN", "HO0", "O0X:JXQ7", ":><2:I<P", "Z?WDQ1", "CL6JL", ">LKHNHQU", "C", "KYU9MIPK@", "4", "4:18ALT", "ZX", "Z2P8X", "KG7IATN", "H>;KB", "74", "@EFL2X", "RW", "F3C", "6CL", "<DV608A", "2N;6", "3LGOND8", "S<VV7?DJ", "LVDZ1", "6VD8I", ">", ":B5B", "IJXT", "RNF", "K4JOC", "MXC", "RXC?39A>4", "P4H>EA", "O", "K=6", "WUJB", "FU9=Q44=", "FKV", "?B", "5WB:KBU", "01TVKNT", "GFWQJC2", "AM:VDM@1", ":", "KL50M", "T", ":F;TE", "7:", "2", "P", "UCR=H6S", "A381L", "6;X5", "VA9?JT", "ZVR7>", "J5U6OL", "4O", "J@ZP3V>", "P7MHQYJP", "TWJXE7R3", "BQ[", "H1", "FQEUGY", "N2ID", "7L:D[T@D", "J[>", "P=K", "R1O[ZHG?I", "U75", "RQF:P", "3LRMK", "NTUQHK:O", "L;JC=S<X", "HN6GAW>D", "4IK", "PCZEC2", "75TVOP7", "0G?1N", "H3UV[", "BHWG<RF:", "HBUMN", "TVS", "W>J@0H", "GA:Z", "4GI", ";?6E16", ">", "FFK@=", "OKW34CC3<", "2B:", "[>", "RXRT2", "6U3LXX", "9KE57", "0", "LQ8", "7VO6L0", "H:8JS?C?C", ";", "D>R1E?2H", "S8JZ2QHNQ", ":C5<;DO2?", "=LB3BDI9Z", "59QO7;D", "AN", "K5PWTL2V", "DKH", "O[", "PL8=", "AFD0Q", "76G;94CH", "H2;", "Z?H<", "[", "NL7[A1", ">1NOM", ":XYR?EO", "[C?Y:@", "8S8VCTU", "URK>XR[", "D?", ":7", "BFEUV", "GNGOE8IA", "VNU4=7D3", "4GZM", "5KKLHXZBC", "AJII7Z", "ELVIAF1X", "IWGNM", "=PQ=MO", "4SIO@", ">", "LTCFU=DW9", "X5", "K:", "@Z[NK", "U8YBW", "1", "H", "ED", "?Q4NZ5J", "NMQ@28?", "SEI[V0>LE", "8JYL9<UQ", "<WES161", "8", "9X07ZD", "B:MW:>ELB", "@<X", "H;<SP", "T6G", "T", "F[UB0;L6J", "D3[>", "?J2JJQ>", "E9N4P8", "36WY", "W84E;", "YRV", "D", ">F", "A;3;9<A=", "1MGKHXKW", "8AT3RJZ", "G4=1", "NQCAB7:", "3PT3S@CW", "03H", "6?WH=BLJ", "EP4", "5F0TM30E", "S=FN1F", "ISX@6XN0", "<?21DB", "BBV4ZV", "0N", "FTT", "JUPZ:V", "@>WLMY5I@", "0:248A", "MGX", "?PLAJ", "?<NN", "0IP6GVO?F", "10;NA", "?X9ZPBK5D", "YPJKTJ8H8", "VXZ@3D", "S6:LYK=W", "OZD7VA?DD", "F", "8O<2", "OW67PY", "B1", "1?I@F0=N", "6E>35T", "=GXS", "36Q", "V6[Q85R", "OK", "O@7IZ", "?1", "79", "OJ>QMYF@;", "S0=YST4", "=;:Y<82", ":<RWSYE", "O7FMG:R>", "3O1W", "WM2I2>R", "YD@8?K5", "L", "DKZCUP93", "YI7PX<", "F>IP", "[8:7G=PT", "1P1[K>7UU", ":B", "BSWM=:OE4", "MS=", "HE", "ZFM", "YQ9I", "3H<Y", "6PG<E9CZ", "I3H>P", "S", "[", "RC7[4P", "7QQ5@@=", "LSH3", ">L=>ZEW9", "W", "@RK@", "?U2H3KYC", "P37", "JAK:6YLKU", "98=", "ZXUT", "O>P:", "5M>UPE6R", "90D", "4GD", "P4N:223", "::MZD2GI", "@Q>GGL", "[I?34;E=", "K[U628", "Q=:M", "Q;850F", "HJ=G743P", "HE7K", "1QF91O", ";A=04YJ4", "N4FC", "5H7=", "7", "GR:", "09Q1XO", "QE=UB", "ZR2F<EJUE", "7", "XAAOLY7U", "89>E[", ";", "NJ:DLQ8J", "E354Y1", "R8JO?C>HL", "E", "@8XF", "J", "W?JY7HE<L", "QJ", "B8SQK", "QPOOS3W7J", "R0FI3W", "K@D", ":N>V4FN", "J<3R[R", "2594PC5:", "L59", "ZMB8?P3CO", "Q=1TG", "2H", "793WLL", "5<7>[5", "AQS6TN=1", "2F7IZ95Y", "?08CL:", "XAVWZR", "OI", "0OK13MH:", "ZX", "W9", "WUF[3G", "XU:", "KC2LQ2@", "G", "FG5MF1", "UN09HZ8K", "H0?:>Z", "@KCVC<B>:", "WWCA<=3", "ZK", "6OLN", "CAEB2VEY8", "9J6XH1", "CBH", "F<6IC", "<4WCG<AA?", ">PWNBFT>", ">IA", "5JN", "8<", "Y0AUX", "5=RX055WS", "U4U?BB9J", "S=V[I7AI", ":EZS", "P8:==JI<", "M5;G[D5C", "B=", "CE[129ZD", "ML5WZVY", "6ULO47N", "P677WQJ", "Q3VBF>", "7", "DFA>DXX", "HKU", ">=WX", "FSM", "LB4ZUJ", "898M8IC", "Y@AYCN4Q", "C5?ZA0G", "W3CP68", "?TEGUYKRQ", "O86;BIO", "@5TA4=<79", "=U", "5", ">4GO", ">XLEH;9Z<", "BME:FI", ":8QNF", "Z3P", "7<WS2;D[T", "8:C4L4IW", "JW<S4B", "6", "TWICKY", "M87L", ">", "OQAS>", "6", "E1M<I3", "PXQ3", "96?X;[3WD", "LKJ2BXYI", "V", "ZKLE4", "V", "?<GE3C", "2F3Z?PI", ":0", "KIKEK1", "OFMGTG>H2", "KYGN", "KL:S:", "1N", "LOL5R5N", "L?", "X", "TE@K", "=K:8NW", "EI1FOGJB", "7", "Q", "6A8LQFX", ">G?3J1;<", "5", "Y7OLN=2", "EOF>U?[", "5YA?", "A6?[83", "H8EZS", "J", "Z5OW", "9NMIM", "N", "DE>=", "NQPQE<A", ":M3=JFBQ", "N", "3S08B6", "?KIL7OUWX", "8>", "V", "<7", "R1;", "O", "7GNTA", "3PW", "UGTN", "P1JF:YYF", "CQJNA=@", "TF@:RW", "50", "L<B3", "C", "C52@3K", "T9Y9ZQ:", ";2Y1Z?7B3", "6EVJ@", "1EFV0I5", ":GF9<8DH;", "1M8L3OD", "8R;", ":QY", "K", ":8SEOQ6DZ", "D9", "E>36AS", "FC<QDGZU", "1SBN2?ATE", ":GQCB", "QF=JQ?4", "K=HJPB:Q", "MWP0=0Z2:", "T5X", ">V<@3", "XO", "M<QANCK", "4FG", ";[G>MDJ", "@", "H", "=XQTTEZ", "97W?KZ67E", "885OF;LH1", "I27", "Z@", "S", "IQF84=S3", "C0J", "87", "7BC7X?", "ZF3XWN7", ";@7WED", "0@J", "ZML", "X;0", "CX7L>M7", "0UVMH;<", "88K", "IML[>P1;", "22WB9", "Q", "6DSDRX", "E8XMU[UB4", "UWNWJ6A4", ";>0D56@@", "7=3LEC>", "WKL[I", ">;U", "A:1BYSV", "XD", "<3", "IJ2GETQ@", "BT0@6O934", "HZT?T:;QE", "<V1VX", "SPQ", "LKD4[2", "8", "XG5?", "DK27@[K", "VLZR", "R[", "BKYVO", "XGIZX43;X", ">COOB>?", "BRGZHZ", "ZIM", "PILP", "J47N", "G", "5BATU8LI2", "H1BFQTGB4", "CP", "FSHMY", "8KXKD", "X4=Y77Z", "IXU", "DOY8J5", "4X4QU", "BX;E", "DUE", ":4@8PY5IR", "PO", "?=", "C9UMG8", "CC0<", "=RIGVZP3?", "4JRUQC8", "L", "PY<;5S6", ";", "@H9", "HG@<JO", "O=S0", "N8", ">S", ";", "H@", "7TGJ9G", "73", "GSU", "T", "030GQL", "GWBR", "BJOIC", "74", "V;=5:4", "@AVTD>", "NZ", "M=RD", "8FXRAJA", "SP84=V", "O;213Z", "W4Q", "R2IN;9", "K[", "9", "311P", ">J:1U<FY;", "<?F350", "S<=DWQF", "2123", "KA@V", "9J@J=", "VMKA", "KEF?;", ";D", "<FY>1OYV3", "PPVH?KM9<", "K[?H", "O;?B", "ONML", "NSA", ">5", "3CPB2U3", "I6", "A7", "4BZZJ", "L;Q4Z1YZ", "F7B", "W", "DQT;RZE", "I4O0FMJK", "=T=;U;N9Q", "378@KA", ">LJ;S", "?GL", "LN", "B[POOJ", "U;L6>S>", "V8>8TA", "31:JM", "O?", "5R7TE9O", "D?X", "6;Y", "W", "LO5", "R6QU", "<H27", "T>A", "04", "DCE;", "8ME3A5:", "LD4A", "?5QA<V93P", "3T0G", "Y", "QIX;L=@", };
    dict_t dict = dict(int);
    unsigned long long start, end;

    start = reckon_usec();
    for (int i = 0; i < TEST_COUNT; i++)
    {
        dict_insert(dict, name[i], &i);
    }
    end = reckon_usec();
    printf("insert %llu\r\n", (unsigned long long)(end - start));

    start = reckon_usec();
    for (int i = 0; i < TEST_COUNT; i++)
    {
        dict_at(dict, int, name[rand() % TEST_COUNT]);
    }
    end = reckon_usec();
    printf("at %llu\r\n", (unsigned long long)(end - start));

    _dict(dict);
#endif 
}
#endif 

static void test_dict(void) 
{
    dict_t dict = dict(int);
    int value;

    value = 100; dict_insert(dict, "hello", &value);
    value = 1; dict_insert(dict, "ZhangSan", &value);
    value = 2; dict_insert(dict, "LiSi", &value);
    value = 3; dict_insert(dict, "WangWu", &value);
    value = 4; dict_insert(dict, "SunLiu", &value);
    value = 5; dict_insert(dict, "QianQi", &value);
    value = 8; dict_insert(dict, "WangBa", &value);
    value = 9; dict_insert(dict, "LiuJiu", &value);
    // dict_show(dict);
    // printf("-------------------------------------------------------------\r\n");
    // printf("size = %d, value size = %d\r\n", dict_size(dict), dict_dsize(dict));
    // dict_erase(dict, "hello");
    // dict_erase(dict, "ZhangSan");
    // dict_erase(dict, "LiSi");
    // dict_erase(dict, "WangWu");
    // dict_clear(dict);
    // dict_show(dict);
    // dict_print(dict);

    printf("dict[ZhangSan] = %d\r\n", dict_at(dict, int, "ZhangSan"));
    printf("dict[hello] = %d\r\n", dict_at(dict, int, "hello"));
    printf("dict[SunLiu] = %d\r\n", dict_at(dict, int, "SunLiu"));
    printf("dict[LiuJiu] = %d\r\n", dict_at(dict, int, "LiuJiu"));

    _dict(dict);
    
    dict = dict(int);
    dict_set_klength(dict, sizeof(int), NULL);
    
    dict_insert(dict, literal(int, 0), literal(int, 100));
    dict_insert(dict, literal(int, 6), literal(int, 1024));
    dict_insert(dict, literal(int, 5), literal(int, -1));
    dict_insert(dict, literal(int, 9), literal(int, 314));
    printf("%d\r\n", *(int *)dict_insert(dict, literal(int, 100), literal(int, 314)));
    
    
    dict_at(dict, int, literal(int, 1000)) = 11111;
    printf("dict[5] = %d\r\n", dict_at(dict, int, literal(int, 5)));
    printf("dict[0] = %d\r\n", dict_at(dict, int, literal(int, 0)));
    printf("dict[6] = %d\r\n", dict_at(dict, int, literal(int, 6)));
    printf("dict[9] = %d\r\n", dict_at(dict, int, literal(int, 9)));
    
    _dict(dict);
}

static void test(void) 
{
    test_dict();
    // test_it();
    // test_performance();
    // v_check_unfree();
    // printf("mem use %d\r\n", v_check_used());
}
init_export_app(test);
