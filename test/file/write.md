|        | Zhang San     |    Li Si     |             Wang Wu |
|--------|:--------------|:------------:|--------------------:|
| age    | 18            |      24      |                  20 |
| gender | man           |    woman     |                 man |
| height | 178.5         |     165      |                 175 |
| weight | 65            |      48      |                  75 |
| email  | 123321@qq.com | lisi@163.com | ww1234567890@qq.com |
