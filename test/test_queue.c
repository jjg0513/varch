#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "tool.h"
#include "valloc.h"
#include "queue.h"

static void test_queue(void) 
{
    queue_t queue = queue(int, 10);
    int i = 0;

    for (i = 0; i < queue_capacity(queue); i++)
    {
        queue_push(queue, &i);
    }
    queue_pop(queue, NULL);
    queue_pop(queue, NULL);
    printf("queue capacity=%d, size=%d, dsize=%d\r\n", queue_capacity(queue), queue_size(queue), queue_dsize(queue));
    for (i = 0; i < queue_size(queue); i++)
    {
        printf("queue[%d] = %d\r\n", i, queue_at(queue, int, i));
    }

    _queue(queue);
}

static void test(void) 
{
    test_queue();
    v_check_unfree();
}
init_export_app(test);
