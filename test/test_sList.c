#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "tool.h"
#include "valloc.h"
#include "sList.h"

static void test_create(void);
static void test_set(void);
static void test_insert(void);
static void test_erase(void);
static void test_attach(void);
static void test_detach(void);
static void test_push(void);
static void test_pop(void);
static void test_to(void);
static void test_size(void);
static void test_append(void);
static void test_copy(void);
static void test_reverse(void);

static void test_create(void)
{
    sList *list = NULL;

    list = sList_create();
    if (!list)
    {
        printf("sList_create Fail!\r\n");
        return;
    }

    printf("sList_create Success!\r\n");

    sList_delete(list);
}

static void test_set(void)
{
    sList *list = NULL;
    int dataInt = 3;
    char *dataString = "Hello sList";
    char outBuffer[32];

    list = sList_create();
    if (!list)
    {
        printf("sList_create Fail!\r\n");
        return;
    }

    printf("sList_create Success!\r\n");

    sList_set(list, &dataInt, sizeof(dataInt));
    printf("list->data %d\r\n", sList_ref(list, int));

    dataInt = 100;
    sList_set(list, &dataInt, sizeof(dataInt));
    printf("list->data %d\r\n", sList_ref(list, int));

    sList_set(list, dataString, strlen(dataString) + 1);
    printf("list->data %s\r\n", ((char *)(list->data)));

    sList_get(list, outBuffer, sizeof(outBuffer));
    printf("get data %s\r\n", outBuffer);

    sList_delete(list);
}

static void test_insert(void)
{
    sList *list = NULL;

    for (int i = 0; i < 5; i++)
    {
        if (!sList_insert(&list, -1, &i, sizeof(i))) goto FAIL;
    }

    sList_forEach(list, n)
    {
        printf("data %d\r\n", sList_ref(n, int));
    }

FAIL:
    sList_delete(list);
}

static void test_erase(void)
{
    sList *list = NULL;

    for (int i = 0; i < 5; i++)
    {
        if (!sList_insert(&list, -1, &i, sizeof(i))) goto FAIL;
    }

    sList_erase(&list, 0, NULL);

    sList_forEach(list, n)
    {
        printf("data %d\r\n", sList_ref(n, int));
    }

FAIL:
    sList_delete(list);
}

static void test_attach(void)
{
    sList *list = NULL, *a = NULL;

    for (int i = 0; i < 5; i++)
    {
        if (!sList_pushBack(&list, &i, sizeof(i))) goto FAIL;
    }
    for (int i = 0; i < 3; i++)
    {
        if (!sList_pushBack(&a, &i, sizeof(i))) goto FAIL;
    }

    sList_attach(&list, -1, a);

    sList_forEach(list, n)
    {
        printf("data %d\r\n", sList_ref(n, int));
    }

FAIL:
    sList_delete(list);
}

static void test_detach(void)
{
    sList *list = NULL, *node;

    for (int i = 0; i < 10; i++)
    {
        if (!sList_insert(&list, -1, &i, sizeof(i))) goto FAIL;
    }

    node = sList_detach(&list, 3, -1, NULL);
    if (!node)
    {
        printf("sList_detach fail\r\n");
    }

    sList_forEach(node, n)
    {
        printf("node data %d\r\n", sList_ref(n, int));
    }

    sList_delete(node);

    sList_forEach(list, n)
    {
        printf("data %d\r\n", sList_ref(n, int));
    }

FAIL:
    sList_delete(list);
}

static void test_push(void)
{
    sList *list = NULL;

    for (int i = 0; i < 5; i++)
    {
        if (!sList_pushFront(&list, &i, sizeof(i))) goto FAIL;
    }
    for (int i = 0; i < 5; i++)
    {
        if (!sList_pushBack(&list, &i, sizeof(i))) goto FAIL;
    }

    sList_forEach(list, n)
    {
        printf("data %d\r\n", sList_ref(n, int));
    }

FAIL:
    sList_delete(list);
}

static void test_pop(void)
{
    sList *list = NULL;

    for (int i = 0; i < 5; i++)
    {
        if (!sList_pushFront(&list, &i, sizeof(i))) goto FAIL;
    }
    for (int i = 0; i < 5; i++)
    {
        if (!sList_pushBack(&list, &i, sizeof(i))) goto FAIL;
    }

    sList_popBack(&list);
    sList_popBack(&list);
    sList_popFront(&list);

    sList_forEach(list, n)
    {
        printf("data %d\r\n", sList_ref(n, int));
    }

FAIL:
    sList_delete(list);
}

static void test_to(void)
{
    sList *list = NULL, *node;

    for (int i = 0; i < 10; i++)
    {
        if (!sList_pushBack(&list, &i, sizeof(i))) goto FAIL;
    }

    node = sList_to(list, 6);
    if (!node)
    {
        printf("sList_to fail\r\n");
        goto FAIL;
    }

    printf("data %d\r\n", sList_ref(node, int));

FAIL:
    sList_delete(list);
}

static void test_size(void)
{
    sList *list = NULL;

    for (int i = 0; i < 10; i++)
    {
        if (!sList_pushBack(&list, &i, sizeof(i))) goto FAIL;
    }

    printf("size %d\r\n", sList_size(list));
    printf("size %d\r\n", sList_size(sList_to(list, 3)));

FAIL:
    sList_delete(list);
}

static void test_append(void)
{
    sList *list = NULL, *ap = NULL;

    for (int i = 0; i < 10; i++)
    {
        if (!sList_pushBack(&list, &i, sizeof(i))) goto FAIL;
        if (!sList_pushBack(&ap, &i, sizeof(i))) goto FAIL;
    }

    if (!sList_append(list, &ap)) goto FAIL;

    printPoint(ap);

    sList_forEach(list, n)
    {
        printf("data %d\r\n", sList_ref(n, int));
    }

FAIL:
    sList_delete(list);
    sList_delete(ap);
}

static void test_copy(void)
{
    sList *list = NULL, *copy = NULL;

    for (int i = 0; i < 10; i++)
    {
        if (!sList_pushBack(&list, &i, sizeof(i))) goto FAIL;
    }

    copy = sList_copy(list, 2, -1);
    if (!copy)
    {
        printf("sList_copy fail\r\n");
    }

    sList_forEach(copy, n)
    {
        printf("data %d\r\n", sList_ref(n, int));
    }

FAIL:
    sList_delete(list);
    sList_delete(copy);
}

static void test_reverse(void)
{
    sList *list = NULL;

    for (int i = 0; i < 10; i++)
    {
        if (!sList_pushBack(&list, &i, sizeof(i))) goto FAIL;
    }

    if (!sList_reverse(list, sList_front, sList_back))
    {
        printf("sList_reverse fail\r\n");
    }

    sList_forEach(list, n)
    {
        printf("data %d\r\n", sList_ref(n, int));
    }

FAIL:
    sList_delete(list);
}

static void test(void)
{
    printf("sList test!\r\n");

    // test_create();
    test_set();
    // test_insert();
    // test_erase();
    // test_attach();
    // test_detach();
    // test_push();
    // test_pop();
    // test_to();
    // test_size();
    // test_append();
    // test_copy();
    // test_reverse();
    
    v_check_unfree();
}
init_export_app(test);
