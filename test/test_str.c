#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "tool.h"
#include "valloc.h"
#include "str.h"

static void test(void)
{
    str_t s = str("0123456789");
    str_replace(s, 9, 10, "AAAAAAAAAA");
    printf("len %d, %d\r\n", str_length(s), str_capacity(s));
    printf("%s\r\n", _S(s));
    _str(s);
}
init_export_app(test);
