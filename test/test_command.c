#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "tool.h"
#include "valloc.h"
#include "command.h"

int func1(int argc, char *argv[])
{
    printf("I am func1!\r\n");
    printf("argc = %d\r\n", argc);
    for (int i = 0; i < argc; i++)
    {
        printf("argv[%d] = %s\r\n", i, argv[i]);
    }
    return 1;
}

int func2(int argc, char *argv[])
{
    printf("I am func2!\r\n");
    return 1;
}

int func3(int argc, char *argv[])
{
    printf("I am func3!\r\n");
    return 1;
}

static void test(void)
{
    command_export("func1", func1);
    command_export("func2", func2);
    command_export("func3", func3);

    command("cmd -nl");
    printf("--------------------------\r\n");
    command("cmd -ln");
    printf("--------------------------\r\n");
    // command("func2");
    // printf("--------------------------\r\n");
    // command("func3");
    // printf("--------------------------\r\n");
    // command("func1 1 2 3");
    // printf("--------------------------\r\n");
    // command("func1 1\\ 2 3");
    // printf("--------------------------\r\n");
    // command("func1 \"1 2 3\"");
}
init_export_app(test);
