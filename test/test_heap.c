#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "tool.h"
#include "valloc.h"
#include "heap.h"

static int heap_root_min(void *parent, void *child)
{
    if (*(int *)parent < *(int *)child) return 1;
    return 0;
}
static int heap_root_max(void *parent, void *child)
{
    if (*(int *)parent > *(int *)child) return 1;
    return 0;
}
static void test_heap(void)
{
    heap_t h = heap_create(sizeof(int), 11, heap_root_max);
    int i = 0;

    for (i = 0; i < 11; i++)
    {
        heap_push(h, &i);
    }
    printf("size %d\r\n", heap_size(h));
    
    heap_pop(h, NULL);
    i = -9;heap_modify(h, 6, &i);
    i = -100;heap_modify(h, 3, &i);
    i = 1000;heap_push(h, &i);

    while (heap_size(h))
    {
        if (heap_pop(h, &i)) printf("pop %d\r\n", i);
        else printf("pop fail!\r\n");
    }

    heap_delete(h);
}

static void test(void)
{
    test_heap();
    v_check_unfree();
}
init_export_app(test);
