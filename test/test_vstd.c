#include "init.h"
#include "vstdlib.h"
#include "vmem.h"

int printf (const char *__format, ...);

static void test(void)
{
    uint8_t *p;

    printf("used %d\r\n", vmem_used());

    p = v_malloc(2048);
    if (!p)
    {
        printf("malloc fail!\r\n");
        return;
    }
    else
    {
        printf("malloc success!\r\n");
    }

    printf("used %d\r\n", vmem_used());

    v_free(p);

    printf("used %d\r\n", vmem_used());
}
init_export_app(test);
